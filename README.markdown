This is a plugin for SBT which allows you to configure and run FindRealBugs Analysis on your scala project.

First you need to create a file named frb.sbt in your /project folder with this content:
<code>
addSbtPlugin( "de.opal-project" % "frbsbtplugin" %  "1.0-SNAPSHOT" intransitive() )
</code>

Then you can import the configurations by:
<code>
org.opalj.frb.sbtplugin.FindRealBugsSbtPlugin.frbSettings
</code>

The two most important setting keys you need to change are the location of your classfiles to be analysed and which analysis you want to run.
If you don't configure any of these options the defaults would be the classDirectory for your project and ALL the analysis will be run.
For example:
<code>
analysisList := Array\[String\]("UselessIncrementInReturn","UrUninitReadCalledFromSuperConstructor","BadlyOverriddenAdapter","CatchesIllegalMonitorStateException","CloneDoesNotCallSuperClone")
</code>

And:
<code>
classFiles := "/absolute/path/to/your/folder/"
</code>

And in the end you can even specify libraries to be added to FRB when analizing your project by adding those files using:
<code>
libraryList := "path/to/your/list/of/files/or/jars"
</code>
You can also set any of these parameters while sbt is in interactive mode by using set command for example:
<code>
set analysisList := Array("NameOfYourAnalysis")
</code>
