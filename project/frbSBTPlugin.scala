/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
/**
 *  This is a project definition in which we separetely load the scala 2.11 dependencies of frb to a separate classpath
 *  and then extract the jars from them to the resource folder so that later they can be packaged along with the rest
 *  of the code.
 *  @author Babak Moghimi
 * *
 */

import sbt._
import Keys._
import Classpaths.managedJars

object frbSBTPlugin extends Build {
    
    lazy val jarsToExtract = TaskKey[Seq[File]]("jars-to-extract", "JAR files to be extracted")

    lazy val extractJarsTarget = SettingKey[File]("extract-jars-target", "Target directory for extracted JAR files")

    lazy val extractJars = TaskKey[Unit]("extract-jars", "Extracts JAR files")

    lazy val removeScala = TaskKey[Unit]("remove-scala", "removes 2.11 scala jars from dependency")

    lazy val cleanResources = TaskKey[Unit]("clean-resources", "Removes everything from resources directory prior to extraction")

    lazy val frbPlugin = Project("frbPlugin", file("frb"), settings = Defaults.defaultSettings ++ Seq(
        // define dependencies
        libraryDependencies ++= Seq(
            "de.opal-project" % "findrealbugs-cli_2.11" % "0.0.1-SNAPSHOT",
            "de.opal-project" % "abstract-interpretation-framework_2.11" % "0.0.1-SNAPSHOT",
            "de.opal-project" % "bytecode-infrastructure_2.11" % "0.8.0-SNAPSHOT",
            "de.opal-project" % "findrealbugs-analyses_2.11" % "0.0.1-SNAPSHOT",
            "de.opal-project" % "bytecode-representation_2.11" % "0.8.0-SNAPSHOT",
            "de.opal-project" % "common_2.11" % "0.8.0-SNAPSHOT",
            "org.scala-lang" % "scala-library" % "2.11.1"
            
        ),

        resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
        
       
        // collect jar files to be extracted from managed jar dependencies
        jarsToExtract <<= (classpathTypes, update) map { (ct, up) ⇒
            managedJars(Compile, ct, up) map { _.data } filter { _.getName match { case arg ⇒ isDependency(arg) } }
        },

        // task to extract jar files
        extractJars <<=
            {
                (cleanResources, jarsToExtract, extractJarsTarget, streams) map { (cleaned, jars, target, streams) ⇒
                    {
                        jars foreach { jar ⇒
                            streams.log.info("Extracting "+jar.getName+" to "+target)
                            IO.unzip(jar, target)
                            IO.delete((target) / "META-INF")
                        }
                    }
                }
            },

        publishArtifact := false,
				
        cleanResources := {
            IO.delete((resourceDirectory in Compile).value)
            IO.createDirectory((resourceDirectory in Compile).value)
        },

        // make it run before compile
        //TODO: make it depend on publish
        compile in Compile <<= (compile in Compile).dependsOn(extractJars)



    )
    )

    /* This function checks the argument against our dependencies and returns true if that argument is in the list
     * The reason is to be able to extract Jar files from the loaded classpath by sbt
     */
    def isDependency(arg: String): Boolean = {
        val dependencies = Array[String]("abstract-interpretation-framework",
            "bytecode-infrastructure", "bytecode-representation", "common_2.11", "findrealbugs-analyses",
            "findrealbugs-cli", "scala-library-2.11", "scala-reflect-2.11", "scala-parser-combinators_2.11",
            "scala-xml_2.11")
        dependencies.exists(arg.contains(_))
    }
}
